' This program is free software; you can redistribute it and/or modify it
' under the terms of the GNU General Public License as published by the Free
' Software Foundation; either version 2 of the License, or (at your option)
' any later version.
'
' This program is distributed in the hope that it will be useful, but WITHOUT
' ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
' FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
' more details.
'
' You should have received a copy of the GNU General Public License along
' with this program; if not, write to the Free Software Foundation, Inc.,
' 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
'
DECLARE FUNCTION Sort% (String1$, String2$)
DECLARE SUB Fin (SndState%, Lang%)
DECLARE SUB Looser (Word$, SndState%)
DECLARE SUB PrintLogo (num%)
DECLARE SUB FSleep (x%)
DECLARE SUB Setup (Lang%)
DECLARE SUB Delmouth ()
DECLARE SUB Winner (SndState%)
DECLARE FUNCTION Check% (Word$, letter$, Tried$)
DECLARE FUNCTION ReadHeader$ (FileToRead$)
DECLARE SUB PlayGame (SndState%)
DECLARE SUB MakeCfgFile ()

CONST pVer$ = "1.05"
CONST pDate$ = "2007"
DIM SHARED Localisation(0 TO 20) AS STRING

SCREEN 1
IF DIR$("HANGMAN.CFG") = "" THEN CALL MakeCfgFile
OPEN "HANGMAN.CFG" FOR INPUT AS #1
INPUT #1, SndState%, Lang%
CLOSE #1
ReLoad:
SELECT CASE Lang%
  CASE 0   ' [Deutsch] ...Thank you, Eric ;-)
    Localisation(0) = "DE"
    Localisation(1) = "Spielen"
    Localisation(2) = "Ton"
    Localisation(3) = "Sprache"
    Localisation(4) = "Ende"
    Localisation(5) = "Aus"
    Localisation(6) = "An "
    Localisation(7) = "Bitte w�hle ein w�rterbuch:"
    Localisation(8) = "Welcher Buchstabe..."    ' Max. 20 letters!
    Localisation(9) = "Game Over! Das korrekte Wort war:"
    Localisation(10) = "Dr�cke eine Taste..."
    Localisation(11) = "Datei:"
  CASE 1   ' [English]
    Localisation(0) = "EN"
    Localisation(1) = "Play"
    Localisation(2) = "Sound"
    Localisation(3) = "Language"
    Localisation(4) = "Exit"
    Localisation(5) = "Off"
    Localisation(6) = "On "
    Localisation(7) = "Please select a vocabulary file:"
    Localisation(8) = "Choose a letter..."     ' Max. 20 letters!
    Localisation(9) = "Game Over! The correct word was:"
    Localisation(10) = "Press any key..."
    Localisation(11) = "File:"
  CASE 2   ' [Francais]
    Localisation(0) = "FR"
    Localisation(1) = "Jouer"
    Localisation(2) = "Son"
    Localisation(3) = "Langue"
    Localisation(4) = "Quitter"
    Localisation(5) = "Non"
    Localisation(6) = "Oui"
    Localisation(7) = "Choisis un fichier de vocabulaire:"
    Localisation(8) = "Choisis une lettre.."   ' Max. 20 letters!
    Localisation(9) = "Fin de la partie! La r�ponse �tait:"
    Localisation(10) = "Appuis sur une touche..."
    Localisation(11) = "Fichier:"
  CASE 3   ' [Magyar] ...Thank you, MegaBrutal ;-)
    Localisation(0) = "HU"
    Localisation(1) = "Uj jatek"
    Localisation(2) = "Hang"
    Localisation(3) = "Nyelv"
    Localisation(4) = "Kilepes"
    Localisation(5) = "Ki"
    Localisation(6) = "Be"
    Localisation(7) = "Valaszd ki a szotarfajlt:"
    Localisation(8) = "Valassz egy betut..."     ' Max. 20 letters!
    Localisation(9) = "Vege a jateknak! A helyes szo:"
    Localisation(10) = "Nyomj meg egy billentyut..."
    Localisation(11) = "Fajl:"
  CASE 4   ' [Nederlands]
    Localisation(0) = "NL"
    Localisation(1) = "Spelen"
    Localisation(2) = "Geluid"
    Localisation(3) = "Taal"
    Localisation(4) = "Afsluiten"
    Localisation(5) = "Uit"
    Localisation(6) = "Aan"
    Localisation(7) = "Kies een woordenboek:"
    Localisation(8) = "Kies een letter..."     ' Max. 20 letters!
    Localisation(9) = "Game Over! Het juiste woord was:"
    Localisation(10) = "Druck een toets om verder te gaan..."
    Localisation(11) = "Bestand:"
  CASE 5   ' [Polski]
    Localisation(0) = "PL"
    Localisation(1) = "Graj"
    Localisation(2) = "Dzwiek"
    Localisation(3) = "Jezyk"
    Localisation(4) = "Wyjscie"
    Localisation(5) = "Off"
    Localisation(6) = "On "
    Localisation(7) = "Wybierz plik slownikowy:"
    Localisation(8) = "Wybierz litere..."
    Localisation(9) = "Koniec gry! Poprawna odpowiedz to:"
    Localisation(10) = "Nacisnij dowolny klawisz..."
    Localisation(11) = "Plik:"
END SELECT

choice% = 1
CLS
COLOR 1
LOCATE 25, 1: PRINT " Copyright (C) Mateusz Viste "; CHR$(34); "Fox"; CHR$(34); " "; pDate$;

FOR x% = 0 TO 7
  CALL PrintLogo(x%)
NEXT x%

LOCATE 11, 18: PRINT "�������������Ŀ";
LOCATE 12, 18: PRINT "�             �";
LOCATE 13, 18: PRINT "�             �";
LOCATE 14, 18: PRINT "�             �";
LOCATE 15, 18: PRINT "�             �";
LOCATE 16, 18: PRINT "�             �";
LOCATE 17, 18: PRINT "�             �";
LOCATE 18, 18: PRINT "���������������";

LOCATE 8, 19: PRINT "Hangman v"; pVer$
LOCATE 13, 22: PRINT Localisation(1);
LOCATE 15, 22: PRINT Localisation(3);
LOCATE 16, 22: PRINT Localisation(4);
GoAgain:
LOCATE 14, 22: PRINT Localisation(2); " "; Localisation(SndState% + 5);

DO
    IF LastKey$ = CHR$(0) + "H" THEN choice% = choice% - 1
    IF LastKey$ = CHR$(0) + "P" OR LastKey$ = CHR$(9) THEN choice% = choice% + 1
    IF choice% < 1 THEN choice% = 4
    IF choice% > 4 THEN choice% = 1
    LOCATE 13, 20: IF choice% = 1 THEN PRINT "*";  ELSE PRINT " ";
    LOCATE 14, 20: IF choice% = 2 THEN PRINT "*";  ELSE PRINT " ";
    LOCATE 15, 20: IF choice% = 3 THEN PRINT "*";  ELSE PRINT " ";
    LOCATE 16, 20: IF choice% = 4 THEN PRINT "*";  ELSE PRINT " ";
    DO: SLEEP: LastKey$ = INKEY$: LOOP UNTIL LastKey$ <> ""
    IF LastKey$ = CHR$(27) THEN choice% = 4: LastKey$ = CHR$(13)
LOOP UNTIL LastKey$ = CHR$(13)
IF choice% = 1 THEN CALL PlayGame(SndState%)
IF choice% = 2 THEN IF SndState% = 1 THEN SndState% = 0 ELSE SndState% = 1
IF choice% = 2 THEN GOTO GoAgain
IF choice% = 3 THEN CALL Setup(Lang%)
IF choice% = 3 THEN GOTO ReLoad
IF choice% = 4 THEN CALL Fin(SndState%, Lang%)
GOTO ReLoad

FUNCTION Check% (Word$, letter$, Tried$)
valeur% = 0
FOR x% = 1 TO LEN(Word$)
      IF UCASE$(MID$(Word$, x%, 1)) = UCASE$(letter$) THEN valeur% = 1
NEXT x%
IF ASC(UCASE$(letter$)) < 65 OR ASC(UCASE$(letter$)) > 90 THEN valeur% = -1
FOR x% = 1 TO LEN(Tried$) - 1
      IF UCASE$(MID$(Tried$, x%, 1)) = UCASE$(letter$) THEN valeur% = -1
NEXT x%
Check% = valeur%
END FUNCTION

SUB Delmouth
LINE (232 - 15, 60 + 24)-(232 + 15, 60 + 24), 1
CIRCLE (232, 60 + 28), 15, 1, 0 * 3.1415 / 180, 180 * 3.1415 / 180, .4
CIRCLE (232, 60 + 22), 15, 1, 180 * 3.1415 / 180, 0 * 3.1415 / 180, .4
END SUB

SUB Fin (SndState%, Lang%)

SCREEN 0
WIDTH 80
CLS
PRINT
PRINT "          HANGMAN V"; pVer$; " COPYRIGHT (C) MATEUSZ VISTE "; CHR$(34); "FOX"; CHR$(34); " 2005-"; pDate$
PRINT
PRINT " ---------------------------------------------------------------------------"
PRINT
PRINT " This program  is free software;  you can  redistribute it  and/or modify it"
PRINT " under the terms of the  GNU General Public License as published by the Free"
PRINT " Software Foundation;  either version 2 of the License,  or (at your option)"
PRINT " any later version."
PRINT
PRINT " This program is distributed in the hope that it will be useful, but WITHOUT"
PRINT " ANY  WARRANTY;  without even  the implied  warranty of  MERCHANTABILITY  or"
PRINT " FITNESS FOR A PARTICULAR  PURPOSE.  See the GNU  General Public License for"
PRINT " more details."
PRINT
PRINT " You should  have received  a copy of  the GNU General Public  License along"
PRINT " with this program;  if not,  write to the  Free Software Foundation,  Inc.,"
PRINT " 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA."
PRINT
PRINT " ---------------------------------------------------------------------------"
PRINT " Hangman's homepage  ->  http://mateusz.viste.free.fr/dos"
PRINT
OPEN "HANGMAN.CFG" FOR OUTPUT AS #1
PRINT #1, SndState%, Lang%
CLOSE #1

SYSTEM

END SUB

SUB FSleep (x%)
FOR z% = 1 TO x%
    t! = TIMER
    DO: LOOP UNTIL t! <> TIMER
NEXT z%
DO: LOOP UNTIL INKEY$ = ""
END SUB

SUB Looser (Word$, SndState%)
PALETTE 2, 4: PALETTE 1, 4: COLOR 4

IF SndState% = 1 THEN
    FOR x% = 600 TO 100 STEP -15
        SOUND x%, 1
    NEXT x%
END IF
CALL FSleep(10)

CLS
PRINT : PRINT Localisation(9): PRINT : PRINT Word$;
LOCATE 25, 1: PRINT Localisation(10): DO: SLEEP: LOOP UNTIL INKEY$ <> ""
END SUB

SUB MakeCfgFile
OPEN "HANGMAN.CFG" FOR OUTPUT AS #1
PRINT #1, 0, 1
CLOSE #1
END SUB

SUB PlayGame (SndState%)
DIM VocFiles(1 TO 20) AS STRING * 12
DIM VocFilesUnsort(1 TO 20) AS STRING * 12
DIM SortTable(1 TO 20) AS STRING
DIM VocHeaders(1 TO 20) AS STRING
RANDOMIZE TIMER * 100
CLS
LOCATE 2, 5: PRINT Localisation(7)

File$ = DIR$(Localisation(0) + "*.VOC")
IF File$ = "" THEN SCREEN 0: WIDTH 80: PRINT "Error: there isn't any vocabulary file in current directory!": SYSTEM

VocNumb% = 0
DO
   VocNumb% = VocNumb% + 1
   VocFilesUnsort(VocNumb%) = UCASE$(File$)
   SortTable(VocNumb%) = ReadHeader$(File$)
   File$ = DIR$
LOOP UNTIL VocNumb% = 20 OR File$ = ""

FOR x% = 1 TO VocNumb%         ' Initializes empty VocHeaders array's values
   VocHeaders(x%) = CHR$(255)  ' to be at the end of the ASCII table.
NEXT x%                        ' Otherwise sorting would fails...

FOR Death% = 1 TO VocNumb%
   FOR x% = 1 TO VocNumb%
      IF Sort%(VocHeaders(x%), SortTable(Death%)) = 0 THEN EXIT FOR
   NEXT x%
   FOR Correct% = VocNumb% TO x% + 1 STEP -1          ' I am using variables
      VocHeaders(Correct%) = VocHeaders(Correct% - 1) ' Death% and Correct%
      VocFiles(Correct%) = VocFiles(Correct% - 1)     ' here only for memory
   NEXT Correct%                                      ' saving purpose!
   VocHeaders(x%) = SortTable(Death%)
   VocFiles(x%) = VocFilesUnsort(Death%)
NEXT Death%

LOCATE 4, 1: PRINT "��������������������������������������Ŀ";
FOR x% = 1 TO VocNumb% + 2
   LOCATE x% + 4, 1: PRINT "�                                      �";
NEXT x%
LOCATE VocNumb% + 7, 1: PRINT "����������������������������������������";

choice% = 1
DO: LastKey$ = INKEY$
  FOR x% = 1 TO VocNumb%
    LOCATE x% + 5, 3
    IF x% = choice% THEN PRINT "*";  ELSE PRINT " ";
    LOCATE x% + 5, 5: PRINT LEFT$(VocHeaders(x%), 25);
    LOCATE x% + 5, 26: PRINT "["; VocFiles(x%); "]";
  NEXT x%
  IF LastKey$ = CHR$(0) + "H" THEN choice% = choice% - 1
  IF LastKey$ = CHR$(0) + "P" THEN choice% = choice% + 1
  IF LastKey$ = CHR$(27) THEN GOTO Finish
  IF choice% = 0 THEN choice% = VocNumb%
  IF choice% > VocNumb% THEN choice% = 1
LOOP UNTIL LastKey$ = CHR$(13)
VocTitle$ = VocHeaders(choice%)
File$ = VocFiles(choice%)

ReStart:
Death% = 0
Correct% = 0
CLS
OPEN File$ FOR INPUT AS #1
Count& = -1  ' Subtraction of the first line (VOC description)
DO
   Count& = Count& + 1
   LINE INPUT #1, Word$
LOOP UNTIL EOF(1)
CLOSE #1

FOR RandSeed% = 0 TO INT(RND * 10)  ' +++++++++++++++++++++++++++++++++++
    x& = INT(RND(1) * Count&) + 1   ' + Here is the random generator... +
NEXT RandSeed%                      ' +++++++++++++++++++++++++++++++++++

OPEN File$ FOR INPUT AS #1
FOR Count& = 0 TO x&
   LINE INPUT #1, Word$
   IF LEN(Word$) > 38 THEN Word$ = LEFT$(Word$, 38)
NEXT Count&
CLOSE #1

PALETTE 2, 9
PALETTE 1, 1
FOR x% = 1 TO 300
      PSET (INT(RND * 320), INT(RND * 200)), 2
NEXT x%
CIRCLE (232, 60), 40, , , , 1.1
CIRCLE (232 - 16, 60 - 9), 8, , , , .7
CIRCLE (232 + 16, 60 - 9), 8, , , , .7
CIRCLE (232 - 16, 60 - 9), 2, 2
CIRCLE (232 + 16, 60 - 9), 2, 2
CIRCLE (232 - 16, 60 - 9), 1, 2
CIRCLE (232 + 16, 60 - 9), 1, 2
CIRCLE (232 - 16, 60 - 11), 10, , 30 * 3.1415 / 180, (180 - 30) * 3.1415 / 180, .6
CIRCLE (232 + 16, 60 - 11), 10, , 30 * 3.1415 / 180, (180 - 30) * 3.1415 / 180, .6
CIRCLE (232, 60 + 2), 9, , 180 * 3.1415 / 180, 360 * 3.1415 / 180, 2.5

CALL PrintLogo(0)

LOCATE 24, 2
FOR Count% = 1 TO LEN(Word$)
   a$ = MID$(Word$, Count%, 1)
   IF (ASC(UCASE$(a$)) > 31 AND ASC(UCASE$(a$)) < 65) OR (ASC(UCASE$(a$)) > 90 AND ASC(UCASE$(a$)) < 97) OR (ASC(UCASE$(a$)) > 122) THEN
      LOCATE 24, 1 + Count%: PRINT a$;
      Correct% = Correct% + 1
    ELSE
      PRINT "_";
   END IF
NEXT Count%
Tried$ = ""
LOCATE 16, 21: PRINT Localisation(8);
LOCATE 18, 21: PRINT "A B C D E F G H I";
LOCATE 19, 21: PRINT "J K L M N O P Q R";
LOCATE 20, 21: PRINT "S T U V W X Y Z";
LINE (140, 0)-(140, 170)
LINE (0, 170)-(320, 170)
LINE (232 - 15, 60 + 24)-(232 + 15, 60 + 24)
LOCATE 1, 1: PRINT Localisation(11); " "; VocTitle$;

DO
ReLoop:
 DO: LastKey$ = INKEY$: LOOP UNTIL LastKey$ <> ""
 IF LastKey$ = CHR$(27) THEN GOTO Finish
 IF ASC(LastKey$) < 32 OR LEN(LastKey$) <> 1 THEN GOTO ReLoop
 LastKey$ = UCASE$(LastKey$)
 Tried$ = Tried$ + LastKey$
 CALL Delmouth
 a% = Check%(Word$, LastKey$, Tried$)
 IF a% >= 0 THEN LOCATE 18 + (ASC(LastKey$) - 65) \ 9, 21 + ((ASC(LastKey$) - 2) MOD 9) * 2: PRINT "*";
 IF a% = 1 THEN
    CIRCLE (232, 60 + 22), 15, , 180 * 3.1415 / 180, 0 * 3.1415 / 180, .4
    IF SndState% = 1 THEN SOUND 1000, 1
 
    FOR x% = 1 TO LEN(Word$)
        IF UCASE$(MID$(Word$, x%, 1)) = UCASE$(LastKey$) THEN Correct% = Correct% + 1: LOCATE 24, 1 + x%: PRINT LastKey$;
    NEXT x%
    IF Correct% = LEN(Word$) THEN CALL Winner(SndState%): GOTO ReStart
  ELSEIF a% = 0 THEN
       CIRCLE (232, 60 + 28), 15, , 0 * 3.1415 / 180, 180 * 3.1415 / 180, .4
       Death% = Death% + 1
       CALL PrintLogo(Death%)
       IF SndState% = 1 THEN SOUND 100, 2
       IF Death% = 7 THEN CALL Looser(Word$, SndState%): GOTO Finish
  ELSE
       Tried$ = LEFT$(Tried$, LEN(Tried$) - 1)
       LINE (232 - 15, 60 + 24)-(232 + 15, 60 + 24)
 END IF
LOOP

Finish:
END SUB

SUB PrintLogo (num%)

SELECT CASE num%
   CASE 0
    LINE (20, 145)-(110, 140) 'Ziemia
    LINE (90, 50)-(88, 140)   'Pal
    LINE (52, 48)-(89, 50)    'Ga���
    LINE (75, 50)-(89, 64)    'Wzmocnienie
   CASE 1
    LINE (52, 48)-(52, 67)    'Sznur
   CASE 2
    CIRCLE (52, 74), 7        'G�owa
   CASE 3
    LINE (52, 80)-(52, 100)   'Korpus
   CASE 4
    LINE (52, 100)-(46, 128)  'Lewa noga
   CASE 5
    LINE (52, 100)-(56, 129)  'Prawa noga
   CASE 6
    LINE (52, 85)-(45, 103)   'Lewa r�ka
   CASE 7
    LINE (52, 84)-(59, 105)   'Prawa r�ka
END SELECT
END SUB

FUNCTION ReadHeader$ (FileToRead$)
  OPEN FileToRead$ FOR INPUT AS #47
  LINE INPUT #47, Header$
  CLOSE #47
  ReadHeader = Header$
END FUNCTION

SUB Setup (Lang%)

DIM Langs(0 TO 5) AS STRING
Langs(0) = "Deutsch"
Langs(1) = "English"
Langs(2) = "Francais"
Langs(3) = "Magyar"
Langs(4) = "Nederlands"
Langs(5) = "Polski"

CLS
LOCATE 4, 14: PRINT "�������������Ŀ";
FOR x% = 1 TO 8
   LOCATE x% + 4, 14: PRINT "�             �";
NEXT x%
LOCATE 13, 14: PRINT "���������������";

choice% = Lang%
DO: LastKey$ = INKEY$
  FOR x% = 0 TO 5
    LOCATE x% + 6, 16
    IF x% = choice% THEN PRINT "*";  ELSE PRINT " ";
    LOCATE x% + 6, 18: PRINT Langs(x%);
  NEXT x%
  IF LastKey$ = CHR$(0) + "H" THEN choice% = choice% - 1
  IF LastKey$ = CHR$(0) + "P" THEN choice% = choice% + 1
  IF LastKey$ = CHR$(27) THEN GOTO AbortMe
  IF choice% < 0 THEN choice% = 5
  IF choice% > 5 THEN choice% = 0
LOOP UNTIL LastKey$ = CHR$(13)
Lang% = choice%
AbortMe:
END SUB

FUNCTION Sort% (tString1$, tString2$)
'
' The Sort function is used to two elements.
'
' Invocation:  Sort%(String1$, String2$)
'
' Result: Sort% = 1 if String1$ and String2$ are already sorted,
'         otherwise Sort% = 0
'
' Example: Sort%("Annie", "Michel") will give 1,
'          Sort%("Monika", "Mathew") will give 0.
'

String1$ = LCASE$(tString1$)
String2$ = LCASE$(tString2$)

IF LEN(String1$) > LEN(String2$) THEN
    String2$ = String2$ + SPACE$(LEN(String1$) - LEN(String2$))
  ELSE
    String1$ = String1$ + SPACE$(LEN(String2$) - LEN(String1$))
END IF

x% = 0
DO: x% = x% + 1
IF x% > LEN(String1$) THEN x% = 1: EXIT DO
IF MID$(String1$, x%, 1) < MID$(String2$, x%, 1) THEN x% = 1: EXIT DO
IF MID$(String1$, x%, 1) > MID$(String2$, x%, 1) THEN x% = 0: EXIT DO
LOOP

Sort% = x%
END FUNCTION

SUB Winner (SndState%)

FOR x! = .7 TO .1 STEP -.08
  CIRCLE (232 + 16, 60 - 9), 8, , , , x!
  CALL FSleep(1)
  CIRCLE (232 + 16, 60 - 9), 8, 0, , , x!
NEXT x!

LINE (232 + 16 - 8, 60 - 9)-(232 + 16 + 8, 60 - 9)

IF SndState% = 1 THEN PLAY "MFMNT200O3E4D8C4C8C4D8E4E8E8D8C8D8E8D8C4O2B8O3MLC2C8": CALL FSleep(5) ELSE CALL FSleep(30)
END SUB

